-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 14 Mei 2018 pada 04.36
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `surpatrol`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `checkpoint`
--

CREATE TABLE `checkpoint` (
  `id` int(10) UNSIGNED NOT NULL,
  `grup` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `checkpoint`
--

INSERT INTO `checkpoint` (`id`, `grup`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'INDOOR', '0A14000AD9', 'B1 - SKILL HALL', NULL, NULL),
(2, 'INDOOR', '0A140004FA', 'B1 - ACCOUNTING', NULL, NULL),
(3, 'INDOOR', '0417519574', 'B1 - AMENITY STORE / FC FILE ROOM', NULL, NULL),
(4, 'INDOOR', '0A14000503', 'B1 - ENGINEERING', NULL, NULL),
(5, 'INDOOR', '041624CA3B', 'B1 - IT OFFICE', NULL, NULL),
(6, 'INDOOR', '041624CB40', 'B1 - SERVICE LIFT', NULL, NULL),
(7, 'INDOOR', '041751D245', 'B1 - STAFF CANTEEN', NULL, NULL),
(8, 'INDOOR', '041624C8B3', 'B1 - STAFF MALE LOCKER', NULL, NULL),
(9, 'INDOOR', '04191F2B25', 'B1 - GENERATOR ROOM / BOILER / TRANSFORMER ROOM', NULL, NULL),
(10, 'INDOOR', '041624C7EA', 'B1 - HRD / LVMDP', NULL, NULL),
(11, 'INDOOR', '040624CA3B', 'IT OFFICE', NULL, NULL),
(12, 'INDOOR', '0A140004DA', 'LV - 10', NULL, NULL),
(13, 'INDOOR', '0A14000AC0', 'LV - 11', NULL, NULL),
(14, 'INDOOR', '0A14000500', 'LV - 14', NULL, NULL),
(15, 'INDOOR', '0A14000ABC', 'LV - 17', NULL, NULL),
(16, 'INDOOR', '0A140004DF', 'LV - 4', NULL, NULL),
(17, 'INDOOR', '0A140004F8', 'LV - 5', NULL, NULL),
(18, 'INDOOR', '0A140004C3', 'LV - 6', NULL, NULL),
(19, 'INDOOR', '0A140004D1', 'LV - 7', NULL, NULL),
(20, 'INDOOR', '0A140004F7', 'LV - 8', NULL, NULL),
(21, 'INDOOR', '0A140004CD', 'LV - 9', NULL, NULL),
(22, 'INDOOR', '0A140004D9', 'LV - 12', NULL, NULL),
(23, 'INDOOR', '0A14000AC2', 'LV - 15', NULL, NULL),
(24, 'INDOOR', '0A14000AC6', 'LV - 16', NULL, NULL),
(25, 'INDOOR', '0A14000ADA', 'LV 1 - BALLROOM', NULL, NULL),
(26, 'INDOOR', '04191F3F2A', 'LV 1 - BANQUET KITCHEN', NULL, NULL),
(27, 'INDOOR', '041624C874', 'LV 1 - BANQUET STORE', NULL, NULL),
(28, 'INDOOR', '041624DB0E', 'LV 1 - MAIN KITCHEN', NULL, NULL),
(29, 'INDOOR', '0A140004E3', 'LV 1 - PURCHASING', NULL, NULL),
(30, 'INDOOR', '04191F42D8', 'LV 1 - SUPER STORE', NULL, NULL),
(31, 'INDOOR', '0A14000B1C', 'LV 1 - SWIMMING POOL', NULL, NULL),
(32, 'INDOOR', '041624DC15', 'LV 2 - NISHIMURA', NULL, NULL),
(33, 'INDOOR', '041624D8EC', 'LV 2 - PELANGI', NULL, NULL),
(34, 'INDOOR', '041624C81D', 'LV 2 - PORTOFINO', NULL, NULL),
(35, 'INDOOR', '0A14000AC4', 'LV 2 - NIRWANA', NULL, NULL),
(36, 'INDOOR', '0A140004FC', 'LV 2 - RECEPTION', NULL, NULL),
(37, 'INDOOR', '041624DB91', 'LV 3 - GM OFFICE', NULL, NULL),
(38, 'INDOOR', '0A140004F3', 'LV 3 - HEALTH CLUB', NULL, NULL),
(39, 'INDOOR', '04191F5447', 'LV 3 - SAUNA & JACCUZI ROOM - FEMALE', NULL, NULL),
(40, 'INDOOR', '04191F1E05', 'LV 3 - SAUNA & JACCUZI ROOM - MALE', NULL, NULL),
(41, 'INDOOR', '04191F44E9', 'LV 3 - SUMATRA ROOM', NULL, NULL),
(42, 'INDOOR', '041624C8FC', 'LV 3 - TENNIS COURT', NULL, NULL),
(43, 'INDOOR', '0A14000ADB', 'LV 1 - DESPERADO', NULL, NULL),
(44, 'INDOOR', '041624CADB', 'LV 1 - JAMOO KITCHEN', NULL, NULL),
(45, 'INDOOR', '041624CD4F', 'ROOF - A', NULL, NULL),
(46, 'INDOOR', '041624C9A4', 'ROOF - B', NULL, NULL),
(47, 'OUTDOOR', '041751A664', 'CARPARK (BUILDING) - LV 2', NULL, NULL),
(48, 'OUTDOOR', '0463000D3A', 'CARPARK (BUILDING) - LV 1', NULL, NULL),
(49, 'OUTDOOR', '0463000D2D', 'CARPARK (BUILDING) - LV 3', NULL, NULL),
(50, 'OUTDOOR', '041751DC8E', 'CARPARK (BUILDING) - LV 4', NULL, NULL),
(51, 'OUTDOOR', '0463000D25', 'CARPARK (BUILDING) - LV 5', NULL, NULL),
(52, 'OUTDOOR', '0463000D28', 'CARPARK (BUILDING) - LV 6', NULL, NULL),
(53, 'OUTDOOR', '041624C900', 'CHEMICAL STORE', NULL, NULL),
(54, 'OUTDOOR', '04191F21FF', 'DRIVER MUSHOLA', NULL, NULL),
(55, 'OUTDOOR', '041624CA90', 'FUEL STATION', NULL, NULL),
(56, 'OUTDOOR', '041624CC3F', 'LV 1 - BALLROOM CAR CALL', NULL, NULL),
(57, 'OUTDOOR', '04191F3603', 'MAIN PARKING AREA - LOT A', NULL, NULL),
(58, 'OUTDOOR', '041751B5E0', 'MAIN PARKING AREA - LOT B', NULL, NULL),
(59, 'OUTDOOR', '04191F2368', 'MAIN PARKING AREA - LOT C', NULL, NULL),
(60, 'OUTDOOR', '041751BD9A', 'MAIN PARKING AREA - LOT D', NULL, NULL),
(61, 'OUTDOOR', '04191F2C0F', 'MAIN PARKING AREA - LOT E', NULL, NULL),
(62, 'OUTDOOR', '04175199A5', 'MAIN PARKING AREA - LOT F', NULL, NULL),
(63, 'OUTDOOR', '041624C8D9', 'MOTORCYCLE KOPERASI', NULL, NULL),
(64, 'OUTDOOR', '0A140004D3', 'POST - 1', NULL, NULL),
(65, 'OUTDOOR', '0A14000AC1', 'POST - 2', NULL, NULL),
(66, 'OUTDOOR', '0A14000AD8', 'POST - 3', NULL, NULL),
(67, 'OUTDOOR', '0A140004F0', 'POST 4 / LPG TANK', NULL, NULL),
(99, '-', '-', '-', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `event`
--

CREATE TABLE `event` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `event`
--

INSERT INTO `event` (`id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, '21', 'STAIR CASE BLOCKING', NULL, NULL),
(2, '1', 'ALARM SOUND', NULL, NULL),
(3, '2', 'BROKEN DOOR', NULL, NULL),
(4, '34', 'CAR TIER DEFLATED / DAMAGE', NULL, NULL),
(5, '32', 'CAR UNLOCK / WINDOW OPEN', NULL, NULL),
(6, '4', 'CEILING LAMP FUSED', NULL, NULL),
(7, '5', 'CHIP / DIRTY TILE', NULL, NULL),
(8, '6', 'DIRTY STANDING ASTHRY', NULL, NULL),
(9, '36', 'DND', NULL, NULL),
(10, '7', 'DOOR KNOP MENU', NULL, NULL),
(11, '33', 'ENGINEERING MAINTENANCE', NULL, NULL),
(12, '8', 'EXIT DOOR BLOCKING', NULL, NULL),
(13, '9', 'FIRE', NULL, NULL),
(14, '10', 'FOUND SUSPECIOUS ITEM', NULL, NULL),
(15, '11', 'HYDRANT LEAKING', NULL, NULL),
(16, '12', 'LAUNDRY BAG', NULL, NULL),
(17, '13', 'LIGHT OFF', NULL, NULL),
(18, '3', 'LOST ACCESORIES', NULL, NULL),
(19, '14', 'LOST ITEM', NULL, NULL),
(20, '15', 'NOISTY / DRUNK', NULL, NULL),
(21, '29', 'NOT IN PLACE PARKING', NULL, NULL),
(22, '16', 'OFFICE LIGHT OFF', NULL, NULL),
(23, '30', 'PANTRY DIRTY', NULL, NULL),
(24, '17', 'PHONE MULFUNCTION', NULL, NULL),
(25, '18', 'PIPE LEAKING', NULL, NULL),
(26, '19', 'SHOES', NULL, NULL),
(27, '20', 'SPOTY / TEAR CARPET', NULL, NULL),
(28, '22', 'TRAFFIC ACCIDENT', NULL, NULL),
(29, '23', 'TROLY / TRAY', NULL, NULL),
(30, '24', 'UNLOCKED DOOR', NULL, NULL),
(31, '25', 'VANDALISME', NULL, NULL),
(32, '26', 'WALL LAMP FUSED', NULL, NULL),
(33, '27', 'WATCH ALARM', NULL, NULL),
(34, '28', 'WINDOW OPEN', NULL, NULL),
(35, '7', 'DOOR KNOP MENU', NULL, NULL),
(36, '188', 'PIPE LEAKING', NULL, NULL),
(37, '131', 'PROJECT / DEKORASI', NULL, NULL),
(38, '31', 'PROJECT / DECORATION ACTIVITY', NULL, NULL),
(39, '331', 'PROJECT ACTIVITY', NULL, NULL),
(40, '223', 'TROLLY', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_12_033806_create_patroli_table', 2),
(6, '2018_05_12_072814_create_event_table', 3),
(7, '2018_05_12_072921_create_checkpoint_table', 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `patroli`
--

CREATE TABLE `patroli` (
  `id` int(10) UNSIGNED NOT NULL,
  `patrolmanid` int(11) NOT NULL,
  `eventid` int(11) NOT NULL,
  `checkpointid` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `shift` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `patroli`
--

INSERT INTO `patroli` (`id`, `patrolmanid`, `eventid`, `checkpointid`, `tanggal`, `shift`, `ket`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, '2018-05-12', 'pagi', '-', '2018-05-12 03:56:35', '2018-05-12 03:56:35'),
(2, 5, 1, 22, '2018-05-12', 'Pagi', '', '2018-05-12 04:52:20', '2018-05-12 04:52:20'),
(3, 5, 1, 1, '2018-05-18', 'Pagi', '', '2018-05-12 04:52:20', '2018-05-12 04:52:20'),
(4, 5, 1, 1, '2018-05-12', 'Pagi', '', '2018-05-12 04:52:20', '2018-05-12 04:52:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `patrolman`
--

CREATE TABLE `patrolman` (
  `id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `patrolman`
--

INSERT INTO `patrolman` (`id`, `nama`) VALUES
(1, 'hartono'),
(2, 'jaenuri'),
(3, 'kastari'),
(4, 'sutaji'),
(5, 'wiji'),
(6, 'ridwan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin@admin.com', '$2y$10$n5nDDgR3WYZWSonYcUaZ0u1rLHh72Zb7gFfIG9lskwp6N3ofkGaAC', 'KQ7PsfugO8zUpFIRUI5HhMqe43RhaJkD5mKxRNUHBWo1XMvfg0NqON4slUwT', '2018-05-11 16:56:59', '2018-05-11 16:56:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `checkpoint`
--
ALTER TABLE `checkpoint`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patroli`
--
ALTER TABLE `patroli`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patrolman`
--
ALTER TABLE `patrolman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `checkpoint`
--
ALTER TABLE `checkpoint`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `patroli`
--
ALTER TABLE `patroli`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `patrolman`
--
ALTER TABLE `patrolman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
