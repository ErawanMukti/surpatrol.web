<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatroliTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patroli', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patrolmanId');
            $table->integer('eventId');
            $table->integer('checkpointId');
            $table->date('tanggal');
            $table->string('jam');
            $table->string('shift', 5);
            $table->text('keterangan');
            $table->text('foto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patroli');
    }
}
