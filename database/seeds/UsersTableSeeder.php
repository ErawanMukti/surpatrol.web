<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Administrator',
                'email' => 'admin@admin.com',
                'password' => '$2y$10$n5nDDgR3WYZWSonYcUaZ0u1rLHh72Zb7gFfIG9lskwp6N3ofkGaAC',
                'remember_token' => 'KQ7PsfugO8zUpFIRUI5HhMqe43RhaJkD5mKxRNUHBWo1XMvfg0NqON4slUwT',
                'created_at' => '2018-05-11 23:56:59',
                'updated_at' => '2018-05-11 23:56:59',
            ),
        ));
        
        
    }
}