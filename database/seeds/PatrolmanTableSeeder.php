<?php

use Illuminate\Database\Seeder;

class PatrolmanTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('patrolman')->delete();
        
        \DB::table('patrolman')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nama' => 'hartono',
            ),
            1 => 
            array (
                'id' => 2,
                'nama' => 'jaenuri',
            ),
            2 => 
            array (
                'id' => 3,
                'nama' => 'kastari',
            ),
            3 => 
            array (
                'id' => 4,
                'nama' => 'sutaji',
            ),
            4 => 
            array (
                'id' => 5,
                'nama' => 'wiji',
            ),
            5 => 
            array (
                'id' => 6,
                'nama' => 'ridwan',
            ),
        ));
        
        
    }
}