<?php

use Illuminate\Database\Seeder;

class CheckpointTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('checkpoint')->delete();
        
        \DB::table('checkpoint')->insert(array (
            0 => 
            array (
                'id' => 1,
                'grup' => 'INDOOR',
                'code' => '0A14000AD9',
                'name' => 'B1 - SKILL HALL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'grup' => 'INDOOR',
                'code' => '0A140004FA',
                'name' => 'B1 - ACCOUNTING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'grup' => 'INDOOR',
                'code' => '0417519574',
                'name' => 'B1 - AMENITY STORE / FC FILE ROOM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'grup' => 'INDOOR',
                'code' => '0A14000503',
                'name' => 'B1 - ENGINEERING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'grup' => 'INDOOR',
                'code' => '041624CA3B',
                'name' => 'B1 - IT OFFICE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'grup' => 'INDOOR',
                'code' => '041624CB40',
                'name' => 'B1 - SERVICE LIFT',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'grup' => 'INDOOR',
                'code' => '041751D245',
                'name' => 'B1 - STAFF CANTEEN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'grup' => 'INDOOR',
                'code' => '041624C8B3',
                'name' => 'B1 - STAFF MALE LOCKER',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'grup' => 'INDOOR',
                'code' => '04191F2B25',
                'name' => 'B1 - GENERATOR ROOM / BOILER / TRANSFORMER ROOM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'grup' => 'INDOOR',
                'code' => '041624C7EA',
                'name' => 'B1 - HRD / LVMDP',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'grup' => 'INDOOR',
                'code' => '040624CA3B',
                'name' => 'IT OFFICE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'grup' => 'INDOOR',
                'code' => '0A140004DA',
                'name' => 'LV - 10',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'grup' => 'INDOOR',
                'code' => '0A14000AC0',
                'name' => 'LV - 11',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'grup' => 'INDOOR',
                'code' => '0A14000500',
                'name' => 'LV - 14',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'grup' => 'INDOOR',
                'code' => '0A14000ABC',
                'name' => 'LV - 17',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'grup' => 'INDOOR',
                'code' => '0A140004DF',
                'name' => 'LV - 4',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'grup' => 'INDOOR',
                'code' => '0A140004F8',
                'name' => 'LV - 5',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'grup' => 'INDOOR',
                'code' => '0A140004C3',
                'name' => 'LV - 6',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'grup' => 'INDOOR',
                'code' => '0A140004D1',
                'name' => 'LV - 7',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'grup' => 'INDOOR',
                'code' => '0A140004F7',
                'name' => 'LV - 8',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'grup' => 'INDOOR',
                'code' => '0A140004CD',
                'name' => 'LV - 9',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'grup' => 'INDOOR',
                'code' => '0A140004D9',
                'name' => 'LV - 12',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'grup' => 'INDOOR',
                'code' => '0A14000AC2',
                'name' => 'LV - 15',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'grup' => 'INDOOR',
                'code' => '0A14000AC6',
                'name' => 'LV - 16',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'grup' => 'INDOOR',
                'code' => '0A14000ADA',
                'name' => 'LV 1 - BALLROOM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'grup' => 'INDOOR',
                'code' => '04191F3F2A',
                'name' => 'LV 1 - BANQUET KITCHEN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'grup' => 'INDOOR',
                'code' => '041624C874',
                'name' => 'LV 1 - BANQUET STORE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'grup' => 'INDOOR',
                'code' => '041624DB0E',
                'name' => 'LV 1 - MAIN KITCHEN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'grup' => 'INDOOR',
                'code' => '0A140004E3',
                'name' => 'LV 1 - PURCHASING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'grup' => 'INDOOR',
                'code' => '04191F42D8',
                'name' => 'LV 1 - SUPER STORE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'grup' => 'INDOOR',
                'code' => '0A14000B1C',
                'name' => 'LV 1 - SWIMMING POOL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'grup' => 'INDOOR',
                'code' => '041624DC15',
                'name' => 'LV 2 - NISHIMURA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'grup' => 'INDOOR',
                'code' => '041624D8EC',
                'name' => 'LV 2 - PELANGI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'grup' => 'INDOOR',
                'code' => '041624C81D',
                'name' => 'LV 2 - PORTOFINO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'grup' => 'INDOOR',
                'code' => '0A14000AC4',
                'name' => 'LV 2 - NIRWANA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'grup' => 'INDOOR',
                'code' => '0A140004FC',
                'name' => 'LV 2 - RECEPTION',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'grup' => 'INDOOR',
                'code' => '041624DB91',
                'name' => 'LV 3 - GM OFFICE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'grup' => 'INDOOR',
                'code' => '0A140004F3',
                'name' => 'LV 3 - HEALTH CLUB',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'grup' => 'INDOOR',
                'code' => '04191F5447',
                'name' => 'LV 3 - SAUNA & JACCUZI ROOM - FEMALE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'grup' => 'INDOOR',
                'code' => '04191F1E05',
                'name' => 'LV 3 - SAUNA & JACCUZI ROOM - MALE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            40 => 
            array (
                'id' => 41,
                'grup' => 'INDOOR',
                'code' => '04191F44E9',
                'name' => 'LV 3 - SUMATRA ROOM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            41 => 
            array (
                'id' => 42,
                'grup' => 'INDOOR',
                'code' => '041624C8FC',
                'name' => 'LV 3 - TENNIS COURT',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            42 => 
            array (
                'id' => 43,
                'grup' => 'INDOOR',
                'code' => '0A14000ADB',
                'name' => 'LV 1 - DESPERADO',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            43 => 
            array (
                'id' => 44,
                'grup' => 'INDOOR',
                'code' => '041624CADB',
                'name' => 'LV 1 - JAMOO KITCHEN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            44 => 
            array (
                'id' => 45,
                'grup' => 'INDOOR',
                'code' => '041624CD4F',
                'name' => 'ROOF - A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            45 => 
            array (
                'id' => 46,
                'grup' => 'INDOOR',
                'code' => '041624C9A4',
                'name' => 'ROOF - B',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            46 => 
            array (
                'id' => 47,
                'grup' => 'OUTDOOR',
                'code' => '041751A664',
            'name' => 'CARPARK (BUILDING) - LV 2',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            47 => 
            array (
                'id' => 48,
                'grup' => 'OUTDOOR',
                'code' => '0463000D3A',
            'name' => 'CARPARK (BUILDING) - LV 1',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            48 => 
            array (
                'id' => 49,
                'grup' => 'OUTDOOR',
                'code' => '0463000D2D',
            'name' => 'CARPARK (BUILDING) - LV 3',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            49 => 
            array (
                'id' => 50,
                'grup' => 'OUTDOOR',
                'code' => '041751DC8E',
            'name' => 'CARPARK (BUILDING) - LV 4',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            50 => 
            array (
                'id' => 51,
                'grup' => 'OUTDOOR',
                'code' => '0463000D25',
            'name' => 'CARPARK (BUILDING) - LV 5',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            51 => 
            array (
                'id' => 52,
                'grup' => 'OUTDOOR',
                'code' => '0463000D28',
            'name' => 'CARPARK (BUILDING) - LV 6',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            52 => 
            array (
                'id' => 53,
                'grup' => 'OUTDOOR',
                'code' => '041624C900',
                'name' => 'CHEMICAL STORE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            53 => 
            array (
                'id' => 54,
                'grup' => 'OUTDOOR',
                'code' => '04191F21FF',
                'name' => 'DRIVER MUSHOLA',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            54 => 
            array (
                'id' => 55,
                'grup' => 'OUTDOOR',
                'code' => '041624CA90',
                'name' => 'FUEL STATION',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            55 => 
            array (
                'id' => 56,
                'grup' => 'OUTDOOR',
                'code' => '041624CC3F',
                'name' => 'LV 1 - BALLROOM CAR CALL',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            56 => 
            array (
                'id' => 57,
                'grup' => 'OUTDOOR',
                'code' => '04191F3603',
                'name' => 'MAIN PARKING AREA - LOT A',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            57 => 
            array (
                'id' => 58,
                'grup' => 'OUTDOOR',
                'code' => '041751B5E0',
                'name' => 'MAIN PARKING AREA - LOT B',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            58 => 
            array (
                'id' => 59,
                'grup' => 'OUTDOOR',
                'code' => '04191F2368',
                'name' => 'MAIN PARKING AREA - LOT C',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            59 => 
            array (
                'id' => 60,
                'grup' => 'OUTDOOR',
                'code' => '041751BD9A',
                'name' => 'MAIN PARKING AREA - LOT D',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            60 => 
            array (
                'id' => 61,
                'grup' => 'OUTDOOR',
                'code' => '04191F2C0F',
                'name' => 'MAIN PARKING AREA - LOT E',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            61 => 
            array (
                'id' => 62,
                'grup' => 'OUTDOOR',
                'code' => '04175199A5',
                'name' => 'MAIN PARKING AREA - LOT F',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            62 => 
            array (
                'id' => 63,
                'grup' => 'OUTDOOR',
                'code' => '041624C8D9',
                'name' => 'MOTORCYCLE KOPERASI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            63 => 
            array (
                'id' => 64,
                'grup' => 'OUTDOOR',
                'code' => '0A140004D3',
                'name' => 'POST - 1',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            64 => 
            array (
                'id' => 65,
                'grup' => 'OUTDOOR',
                'code' => '0A14000AC1',
                'name' => 'POST - 2',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            65 => 
            array (
                'id' => 66,
                'grup' => 'OUTDOOR',
                'code' => '0A14000AD8',
                'name' => 'POST - 3',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            66 => 
            array (
                'id' => 67,
                'grup' => 'OUTDOOR',
                'code' => '0A140004F0',
                'name' => 'POST 4 / LPG TANK',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            67 => 
            array (
                'id' => 99,
                'grup' => '-',
                'code' => '-',
                'name' => '-',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}