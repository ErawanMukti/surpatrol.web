<?php

use Illuminate\Database\Seeder;

class EventTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('event')->delete();
        
        \DB::table('event')->insert(array (
            0 => 
            array (
                'id' => 1,
                'code' => '21',
                'name' => 'STAIR CASE BLOCKING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'code' => '1',
                'name' => 'ALARM SOUND',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'code' => '2',
                'name' => 'BROKEN DOOR',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            3 => 
            array (
                'id' => 4,
                'code' => '34',
                'name' => 'CAR TIER DEFLATED / DAMAGE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            4 => 
            array (
                'id' => 5,
                'code' => '32',
                'name' => 'CAR UNLOCK / WINDOW OPEN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            5 => 
            array (
                'id' => 6,
                'code' => '4',
                'name' => 'CEILING LAMP FUSED',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            6 => 
            array (
                'id' => 7,
                'code' => '5',
                'name' => 'CHIP / DIRTY TILE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            7 => 
            array (
                'id' => 8,
                'code' => '6',
                'name' => 'DIRTY STANDING ASTHRY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            8 => 
            array (
                'id' => 9,
                'code' => '36',
                'name' => 'DND',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            9 => 
            array (
                'id' => 10,
                'code' => '7',
                'name' => 'DOOR KNOP MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            10 => 
            array (
                'id' => 11,
                'code' => '33',
                'name' => 'ENGINEERING MAINTENANCE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            11 => 
            array (
                'id' => 12,
                'code' => '8',
                'name' => 'EXIT DOOR BLOCKING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            12 => 
            array (
                'id' => 13,
                'code' => '9',
                'name' => 'FIRE',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            13 => 
            array (
                'id' => 14,
                'code' => '10',
                'name' => 'FOUND SUSPECIOUS ITEM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            14 => 
            array (
                'id' => 15,
                'code' => '11',
                'name' => 'HYDRANT LEAKING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            15 => 
            array (
                'id' => 16,
                'code' => '12',
                'name' => 'LAUNDRY BAG',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            16 => 
            array (
                'id' => 17,
                'code' => '13',
                'name' => 'LIGHT OFF',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            17 => 
            array (
                'id' => 18,
                'code' => '3',
                'name' => 'LOST ACCESORIES',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            18 => 
            array (
                'id' => 19,
                'code' => '14',
                'name' => 'LOST ITEM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            19 => 
            array (
                'id' => 20,
                'code' => '15',
                'name' => 'NOISTY / DRUNK',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            20 => 
            array (
                'id' => 21,
                'code' => '29',
                'name' => 'NOT IN PLACE PARKING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            21 => 
            array (
                'id' => 22,
                'code' => '16',
                'name' => 'OFFICE LIGHT OFF',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            22 => 
            array (
                'id' => 23,
                'code' => '30',
                'name' => 'PANTRY DIRTY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            23 => 
            array (
                'id' => 24,
                'code' => '17',
                'name' => 'PHONE MULFUNCTION',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            24 => 
            array (
                'id' => 25,
                'code' => '18',
                'name' => 'PIPE LEAKING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            25 => 
            array (
                'id' => 26,
                'code' => '19',
                'name' => 'SHOES',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            26 => 
            array (
                'id' => 27,
                'code' => '20',
                'name' => 'SPOTY / TEAR CARPET',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            27 => 
            array (
                'id' => 28,
                'code' => '22',
                'name' => 'TRAFFIC ACCIDENT',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            28 => 
            array (
                'id' => 29,
                'code' => '23',
                'name' => 'TROLY / TRAY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            29 => 
            array (
                'id' => 30,
                'code' => '24',
                'name' => 'UNLOCKED DOOR',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            30 => 
            array (
                'id' => 31,
                'code' => '25',
                'name' => 'VANDALISME',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            31 => 
            array (
                'id' => 32,
                'code' => '26',
                'name' => 'WALL LAMP FUSED',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            32 => 
            array (
                'id' => 33,
                'code' => '27',
                'name' => 'WATCH ALARM',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            33 => 
            array (
                'id' => 34,
                'code' => '28',
                'name' => 'WINDOW OPEN',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            34 => 
            array (
                'id' => 35,
                'code' => '7',
                'name' => 'DOOR KNOP MENU',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            35 => 
            array (
                'id' => 36,
                'code' => '188',
                'name' => 'PIPE LEAKING',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            36 => 
            array (
                'id' => 37,
                'code' => '131',
                'name' => 'PROJECT / DEKORASI',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            37 => 
            array (
                'id' => 38,
                'code' => '31',
                'name' => 'PROJECT / DECORATION ACTIVITY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            38 => 
            array (
                'id' => 39,
                'code' => '331',
                'name' => 'PROJECT ACTIVITY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
            39 => 
            array (
                'id' => 40,
                'code' => '223',
                'name' => 'TROLLY',
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));
        
        
    }
}