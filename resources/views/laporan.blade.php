@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Laporan Patroli</div>

                <div class="panel-body">
                    {{ Form::open(array('url' => 'pdf', 'method' => 'GET', 'class' => 'form-horizontal form-label-left')) }}
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-10">
                          <a href="{{ url('/home') }}" class="btn btn-default">Kembali</a>

                          {{Form::hidden('id_patrolman', $request['id_patrolman'])}}
                          {{Form::hidden('shift', $request['shift'])}}
                          {{Form::hidden('tgl_mulai', $request['tgl_mulai'])}}
                          {{Form::hidden('tgl_akhir', $request['tgl_akhir'])}}
                          <button type="submit" class="btn btn-danger">Conv. Pdf</button>
                          </div>
                        </div>
                    {{ Form::close() }}
                    <table class="table">
                      <thead>
                        <tr>
                          <th scope="col">No</th>
                          <th scope="col">Tanggal</th>
                          <th scope="col">Jam</th>
                          <th scope="col">Group</th>
                          <th scope="col">Checkpoint</th>
                          <th scope="col">Event</th>
                          <th scope="col">Keterangan</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 1; ?>
                        @foreach($data as $key => $value)
                        <tr>
                          <th scope="row">{{ $i }}</th>
                          <td>{{ date('d-m-Y', strtotime($value->tanggal)) }}</td>
                          <td>{{ $value->jam }}</td>
                          <td>{{ $value->checkpoint->grup }}</td>
                          <td>{{ $value->checkpoint->name }}</td>
                          <td>{{ $value->event->name }}</td>
                          <td>{{ $value->keterangan }}</td>
                        </tr>
                        <?php $i++; ?>
                        @endforeach
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection