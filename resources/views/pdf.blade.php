<table width="100%" style="background-color: #ffffff; filter: alpha(opacity=40); opacity: 0.95;border:2px black solid;">
  <tbody>
    <tr>
      <td class="col-md-2 col-sm-2 col-xs-12"><strong>Company Name:</strong></td>
      <td class="col-md-10 col-sm-10 col-xs-12">SHANGRILA HOTEL</td>
    </tr>
    <tr>
      <td><strong>Address:</strong></td>
      <td>JL MAYJEN SUNGKONO 120 SURABAYA</td>
    </tr>
    <tr>
      <td colspan="2"><center><font size="20"><strong>Patrolman / Tour Report</strong></font></center></td>
    </tr>
    <tr>
      <td><strong>Report Data:</strong></td>
      <td><?php echo date('Y-m-d'); ?></td>
    </tr>
    <tr>
      <td><strong>Report Time:</strong></td>
      <td><?php echo date('H:i:s'); ?></td>
    </tr>    
  </tbody>
</table>

<table width="100%">
  <tbody>
    <tr>
      <td width="20%"><strong>Patrolman:</strong></td>
      <td width="30%">{{ $patrolman->nama }}</td>
      <td width="20%"><strong>Periode Awal:</strong></td>
      <td width="30%">{{ $request['tgl_mulai'] }}</td>
    </tr>    
    <tr>
      <td width="20%"><strong>Shift:</strong></td>
      <td width="30%">{{ $shift }}</td>
      <td width="20%"><strong>Periode Akhir:</strong></td>
      <td width="30%">{{ $request['tgl_akhir'] }}</td>
    </tr>    
  </tbody>
</table>

<p>&nbsp;</p>

<table width="100%">
  <thead>
    <tr>
      <th scope="col"><u>No</u></th>
      <th scope="col"><u>Tanggal</u></th>
      <th scope="col"><u>Jam</u></th>
      <th scope="col"><u>Group</u></th>
      <th scope="col"><u>Checkpoint</u></th>
      <th scope="col"><u>Event</u></th>
      <th scope="col"><u>Keterangan</u></th>
      <th scope="col"><u>Gambar</u></th>
    </tr>
  </thead>
  <tbody>
    <?php $i = 1; ?>
    @foreach($data as $key => $value)
    <tr>
      <th scope="row">{{ $i }}</th>
      <td>{{ date('d-m-Y', strtotime($value->tanggal)) }}</td>
      <td>{{ $value->jam }}</td>
      <td>{{ $value->checkpoint->grup }}</td>
      <td>{{ $value->checkpoint->name }}</td>
      <td>{{ $value->event->name }}</td>
      <td>{{ $value->keterangan }}</td>
      <td><img src="{{ asset('storage/'.$value->foto) }}" width="100px" height="100px"></td>
    </tr>
    <?php $i++; ?>
    @endforeach
  </tbody>
</table>
<p><strong>Total Number of Checkpoint Visited:</strong> {{$i-1}}</p>