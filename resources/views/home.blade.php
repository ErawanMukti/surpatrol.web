@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Filter Laporan</div>
                <div class="panel-body">
                    {{ Form::open(array('url' => 'laporan', 'method' => 'GET', 'class' => 'form-horizontal form-label-left')) }}
                        <div class="form-group">
                          {{ Form::label('id_patrolman', 'Patrolman', array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            {{Form::select('id_patrolman', $patrolman, Input::old('id_patrolman'), array('class' => 'form-control col-md-12 col-xs-12'))}}
                          </div>
                        </div>
                        <div class="form-group">
                          {{ Form::label('shift', 'Shift', array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            {{Form::select('shift', [
                              'Pagi' => 'pagi',
                              'Siang' => 'Siang',
                              'Malam' => 'Malam'
                            ], Input::old('shift'), array('class' => 'form-control col-md-12 col-xs-12'))}}
                          </div>
                        </div>
                        <div class="form-group">
                          {{ Form::label('tgl_mulai', 'Periode Awal', array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            {{Form::date('tgl_mulai', Input::old('tgl_mulai'), array('class' => 'form-control col-md-12 col-xs-12'))}}
                          </div>
                        </div>
                        <div class="form-group">
                          {{ Form::label('tgl_akhir', 'Periode Akhir', array('class' => 'control-label col-md-2 col-sm-2 col-xs-12')) }}
                          <div class="col-md-10 col-sm-10 col-xs-12">
                            {{Form::date('tgl_akhir', Input::old('tgl_akhir'), array('class' => 'form-control col-md-12 col-xs-12'))}}
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-1">
                          {{ Form::submit('Lihat Laporan', array('class' => 'btn btn-success'))}}
                          </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection