<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input, Storage;
use App\Patroli;

class UploadController extends Controller
{
    public function store(Request $request)
    {
        $input = Input::get('hasilpatroli');
        $data = json_decode($input, true);

    	$files = $request->file('gambar');
        $data['foto'] = $files['foto']->store("foto");

    	if ( Patroli::create($data) ) {
	        $res['status']  = 'Ok';
	        $res['message'] = 'Success';
    	} else {
	        $res['status']  = 'Error';
	        $res['message'] = 'Terjadi kesalahan';
    	}

        return response($res);
    }
}
