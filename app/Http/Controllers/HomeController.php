<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patroli;
use App\Patrolman;
use PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Patroli::orderBy('tanggal', 'desc')->get();
        $patrolman = Patrolman::pluck('nama', 'id');
        return view('home', [
            'data' => $data,
            'patrolman' => $patrolman
        ]);
    }

    public function laporan(Request $request)
    {
        $id     = $request['id_patrolman'];
        $shift  = $request['shift'];
        $dawal  = $request['tgl_mulai'];
        $dakhir = $request['tgl_akhir'];

        $data   = Patroli::where('patrolmanid', $id)
                         ->where('shift', $shift)
                         ->whereBetween('tanggal', [$dawal, $dakhir])
                         ->get();

        return view('laporan', [
            'patrolmanid' => $id,
            'shift' => $shift,
            'data' => $data,
            'request' => $request
        ]);
    }

    public function pdf(Request $request)
    {
        $id     = $request['id_patrolman'];
        $shift  = $request['shift'];
        $dawal  = $request['tgl_mulai'];
        $dakhir = $request['tgl_akhir'];

        $data = Patroli::where('patrolmanid', $id)
                       ->where('shift', $shift)
                       ->get();
        $patrolman = Patrolman::find($id);

        $pdf = PDF::loadView('pdf', [
            'data' => $data,
            'patrolman' => $patrolman,
            'shift' => $shift,
            'request' => $request
        ]);
        return $pdf->download('laporan.pdf');
    }}
