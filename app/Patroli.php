<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patroli extends Model
{
    protected $table = 'patroli';

    protected $fillable = ['patrolmanId', 'eventId', 'checkpointId', 'tanggal', 'jam', 'shift', 'keterangan', 'foto'];

    public function checkpoint()
    {
        return $this->belongsTo('App\Checkpoint', 'checkpointId', 'id');
    }

    public function event()
    {
        return $this->belongsTo('App\Event', 'eventId');
    }

    public function patrolman()
    {
        return $this->belongsTo('App\Patrolman', 'patrolmanId');
    }
}
