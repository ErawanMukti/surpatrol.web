<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patrolman extends Model
{
    protected $table = 'patrolman';
}
